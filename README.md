<h1 align="center">Desenvolvimento Frontend</h1>

<p align="center">
  <a href="#projetos">Projetos</a> •
  <a href="#tecnologias">Tecnologias</a> •
  <a href="#como-contribuir">Como Contribuir</a> •
  <a href="#licença">Licença</a>
</p>

<div align="center">
  <sub>O projeto foi desenvolvido por
  <a href="https://github.com/moouro">moouro</a>
  </sub>
</div>

<br />

<!-- Título e descrição do Projeto -->

## Projetos

Este repositório contém os projetos que desenvolvi como parte do meu aprendizado em desenvolvimento frontend. Cada projeto tem uma breve descrição abaixo e um link para sua pasta correspondente neste repositório.

- [Faster shopping](https://gitlab.com/moouro/code-samples/-/tree/main/01-site): Landing page.

## Tecnologias

Nesse repositório, as tecnologias HTML, CSS e JavaScript são utilizadas para criar aplicações web responsivas, visualmente atraentes e interativas. Além disso, são frequentemente usados frameworks e bibliotecas como React, Vue para acelerar o desenvolvimento e fornecer recursos adicionais.

O HTML é utilizado para estruturar o conteúdo da página, o CSS é usado para estilizar e formatar o layout da página, enquanto o JavaScript é responsável por adicionar interatividade e dinamismo às aplicações.

Essas tecnologias são amplamente utilizadas no desenvolvimento web moderno e são essenciais para criar sites, aplicativos que sejam acessíveis e fáceis de usar para todos os usuários. Combinadas com outras ferramentas e tecnologias, elas permitem criar experiências de usuário únicas e personalizadas.

## Como Contribuir

Você pode contribuir com sugestões de melhorias, novas features ou correção de bugs. Para saber mais, leia o arquivo CONTRIBUTING.md.

## Licença

Distribuído sob a licença MIT. Veja `LICENSE` para mais informações.
