# Fast Shopping Landing Page

![Fast Shopping](https://gitlab.com/moouro/code-samples/-/raw/main/01-site/assets/image/site-01.png)

"Este projeto trata-se de uma landing page criada com o objetivo de aprimorar habilidades em frontend. Com um design atraente e funcional, a página tem como finalidade proporcionar tarefas desafiadoras para estudos e práticas no desenvolvimento web. Através da implementação de técnicas avançadas e utilização de tecnologias modernas."

## Deploy

O projeto já está em produção e pode ser acessado através do link [Fast shopping](#).

## Tecnologias utilizadas

- HTML5
- CSS3
- JavaScript

<!-- ## Funcionalidades

A página inclui as seguintes funcionalidades:

- Carrossel de produtos em destaque;
- Listagem dos departamentos disponíveis;
- Seção de promoções;
- Formulário de cadastro para receber novidades;
- Botão de chamada para ação (Call-to-action) para incentivar a compra. -->

## Como executar o projeto

Para executar o projeto localmente, basta clonar este repositório e abrir o arquivo `index.html` no navegador de sua preferência.

```bash
git clone https://github.com/seu-usuario/fast-shopping.git
cd fast-shopping


```
