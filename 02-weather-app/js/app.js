const API_KEY = 'd93d9391b5ef6f3ce7fb3e33712a16d6'
const form = document.querySelector('form')
const search = document.querySelector('#search')
const weather = document.querySelector('#weather')

const carregando = 'Carregando...'
const cidadeNaoEncontrada = 'Cidade não encontrada'

const showWeather = data => {
  if (data.cod == '404') {
    weather.innerHTML = `<h2>${cidadeNaoEncontrada}</h2>`
    return
  }
  // generate image url api

  const temperatura = `${data.main.temp.toFixed(0)} °C`
  const clima =
    data.weather[0].main === 'Clear'
      ? 'Limpo'
      : data.weather[0].main === 'Clouds'
      ? 'Nublado'
      : data.weather[0].main === 'Rain'
      ? 'Chuvoso'
      : data.weather[0].main === 'Thunderstorm'
      ? 'Tempestuoso'
      : data.weather[0].main === 'Drizzle'
      ? 'Garoa'
      : data.weather[0].main === 'Snow'
      ? 'Neve'
      : data.weather[0].main === 'Mist'
      ? 'Névoa úmida'
      : data.weather[0].main === 'Smoke'
      ? 'Fumaça'
      : data.weather[0].main === 'Haze'
      ? 'Neblina seca'
      : data.weather[0].main === 'Dust'
      ? 'Poeira'
      : data.weather[0].main === 'Fog'
      ? 'Nevoeiro'
      : data.weather[0].main === 'Sand'
      ? 'Areia'
      : data.weather[0].main === 'Ash'
      ? 'Cinzas vulcânicas'
      : data.weather[0].main === 'Squall'
      ? 'Temporal'
      : data.weather[0].main === 'Tornado'
      ? 'Tornado'
      : data.weather[0].main
  //Tradução para tipos de clima

  weather.innerHTML = `
    <div>
    <img src="https://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png">
    </div>
    <div>
    <h2>${temperatura}</h2>
      <h4>${clima}</h4>
    </div>
  `
}

const getWeather = async city => {
  weather.innerHTML = `<h2>${carregando}</h2>`

  const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=metric`

  const response = await fetch(url)
  const data = await response.json()

  showWeather(data)
}

form.addEventListener('submit', function (event) {
  getWeather(search.value)
  event.preventDefault()
})
